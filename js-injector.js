<script>
// 2020-12-02 Ed Parsadanyan: JS injector for Shiny-Parser PoC
// - adding node name a its properties in text,
// - node XPATH as tooltip

var inj_ignore = ["inj_ttip", "inj_ttext"];

// main routine to add element name + xpath tooltip
function func(node, xpath) {
   var allowedclass = (node.className !== "" ? 1 : 0);
   
   var prohibitedtags = ["SCRIPT", "STYLE"];
   var allowedtag   = (prohibitedtags.indexOf(node.tagName) !== -1 ? 0 : 1);
   
   var colortags    = ["DIV", "TABLE"];
   var istagcolored = 1; //(colortags.indexOf(node.tagName) !== -1 ? 1 : 0);
   
   var openspan ="";
   var closespan="";
   var lvltxt   ="";
   
    if (istagcolored) {
    	//node.style.border = "thin dotted red";
    	//node.style.padding= 3 + "px";
    	lvltxt = " " + xpath + " " ;
    	openspan  = "<span class=\"inj_ttip\" style=\"background-color:#ddd; font-family: monospace; color: black;\">";
    	closespan = "</span>";
   }
   

   //loop over valid element attributes and add them in a [brackets]
   if(typeof(node.tagName) !== 'undefined') {
   var preftext = openspan + "&lt" + node.tagName.toLowerCase();
     if (node.hasAttributes()) {
       preftext += "[";
       var attrs = node.attributes;
       var attrlist = "";
       var attrvalue = "";
       for(var i = attrs.length - 1; i >= 0; i--) {
          
         //ignore very long attribute values and don't show a.href's
         if((attrs[i].value.length<30 & attrs[i].name !== 'href') | (attrs[i].name == 'class' | attrs[i].name == 'id') ) {
            attrvalue = "@" + attrs[i].name + "='" + attrs[i].value + "'";
         } else { attrvalue = "@" + attrs[i].name; }

         attrlist += attrvalue;
       }
       preftext += attrlist + "]";
     }
   }

   preftext += "&gt";
   // add xpath tooltip
   if(lvltxt.length>1){
      preftext += "<span class=\"inj_ttext\">" + lvltxt + "</span>";
   }
   preftext += closespan;
   
   // tags and tooltips were added inside which caused problems with hyperlinks element:
   // it was not possible to select tooltip
   //if (allowedtag) { node.innerHTML = preftext + node.innerHTML; }
   if (allowedtag & typeof(node.tagName) !== 'undefined') {
         node.insertAdjacentHTML('afterbegin', preftext);
   }
   
   //if (allowedtag) { node.insertAdjacentHTML('beforebegin', preftext); }
}

// loop over all page elements
function walk(node, funcl, xpath) {
   xpath = xpath + "/" + node.tagName;
   xpath = xpath.toLowerCase();
   
   var children = node.childNodes;
   for (var i = 0; i < children.length; i++)  // Children are siblings to each other
   
       //loop over original elements and ignore new <span> tags with element name and xpath tooltip
       if(inj_ignore.indexOf(children[i].className) == -1) { walk(children[i], func, xpath) }
       
   func(node, xpath);
}

walk(document.body,func, "/");

</script>
