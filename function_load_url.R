# AGRA MVP. Shiny-parser proof of concept
# Main functionality, URL page loading
# 10-DEC-2020 by Ed Parsadanyan
# 
# Update history
# 
# 11-DEC-2020 EP: Added html encoding conversion for non UTF-8 pages.

load_url_page <- function(cfg_, session) {
  
  # Remove previously loaded iframe
  if(ReactVals$Iframe_loaded) {
    removeUI(selector ="#iframediv")

    ReactVals$Iframe_loaded <- FALSE
    
    removeUI(selector ="#sortable")
    ReactVals$Sortable_loaded <- FALSE
    
    disable("btnsavetable")
    disable("btnsaveCFG")
    
    ReactVals$parsed_df <- data.frame()
  }
  
  
  options(timeout=30)

  
  isvalidurl     <- "FAILED TO GET URL"
  conttype       <- "INVALID CONTENT"
  statusnum      <- -999
  isvalidcontent <- FALSE
  isvalidstatus  <- FALSE
  urlencoding    <- def_encoding
    
  possibleError <- tryCatch({
    
    isvalidurl     <- GET(cfg_$url, user_agent(uagent_))
    conttype       <- headers(isvalidurl)$`content-type`
    statusnum      <- status_code(isvalidurl)
    isvalidcontent <- str_detect(conttype,"text")
    isvalidstatus  <- between(status_code(isvalidurl),200,399)
    
    # str(isvalidurl)
    
    charset        <- str_locate(isvalidurl$headers$`content-type`,"charset")
    urlencoding    <- ifelse(is.na(charset[[2]]), def_encoding,
                             toupper(str_sub(isvalidurl$headers$`content-type`,charset[[2]]+2))
                             )
    
  }, error=function(e)
  { 
    
    parse_err <- paste0("FAILED TO GET URL :",conditionMessage(e))
    showNotification(parse_err,'',type = "error")
    return()
  }) # tryCatch
  
  
  if(!isvalidstatus) {
    showNotification(paste0("URL CAN NOT BE LOADED. SERVER RETURNED STATUS: ",statusnum),'',type = "error", duration = 8)
  }
  else if(isvalidstatus & !isvalidcontent) {
    showNotification(paste0("UNSUPPORTED CONTENT TYPE: ",conttype,". CONTENT SHOULD CONTAIN TEXT DATA E.G. HTML/XML"),'',type = "error", duration = 8)
  }
  else if(isvalidstatus & isvalidcontent) {
    
    ###########################################################
    #  Load URL, modify with injectors and open in an Iframe  #
    ###########################################################
    
    page_raw <- tempfile()
    
    showNotification(paste0("TRYING TO DOWNLOAD PAGE: ",cfg_$url,". TIMEOUT: ",getOption('timeout')," SEC"),'',type = "message")
    
    download.file(cfg_$url, destfile=page_raw, method="libcurl", quiet = TRUE,
                  headers = c("User-Agent" = uagent_))
    
    f_htmlraw <- file(page_raw, open="r")
    f_htmlraw_lines <- readLines(f_htmlraw)
    close(f_htmlraw)
    
    # Convert encoding if needed
    if(urlencoding != def_encoding ) {
      f_htmlraw_lines <- iconv(f_htmlraw_lines, from=urlencoding, to=def_encoding)
      showNotification("CONVERTING URL ENCODING TO ",urlencoding,type = "message")
    }

    # find closing </body> tag
    body_line   <- which(str_detect(str_to_lower(f_htmlraw_lines),"</body>"))
    
    # Check if body exists at all (i.e. URL is not a pure XML file)
    if(length(body_line)) {
      
      body_symbol <- str_locate(str_to_lower(f_htmlraw_lines[[body_line[1]]]),"</body>")[1]
      
      # Inject CSS and JS
      f_htmlraw_lines[[body_line[1]]] <-
        paste0(
          str_sub(f_htmlraw_lines[[body_line[1]]],1,body_symbol-1),
          paste0(f_css_lines,collapse = "\n"),
          ifelse(cfg_$showtags, paste0(f_js_lines,collapse = "\n"), ""),
          str_sub(f_htmlraw_lines[[body_line[1]]],body_symbol)
        )
    } else {
      showNotification("FAILED TO INJECT CSS/JS CODE. SHOWING THE RAW FILE INSTEAD",'',type = "warning")
    }
    # save modified HTML file
    page_iframe <- tempfile()
    f_htmlinj <- file(page_iframe, open="w", encoding=urlencoding)
    writeLines(f_htmlraw_lines, con=f_htmlinj)
    close(f_htmlinj)
    
    # Load modified HTML file
    htmlpage_ <- read_html(page_iframe, encoding = urlencoding)
    ReactVals$bodynodes <- htmlpage_ # f_htmlraw_lines   # %>% html_node('body') %>% html_children()
    

    # Create sortable lists with all tags, node selector and node attributes
    removeUI("#btnshowreadme", immediate = TRUE)
    
    insertUI(
      selector = '#Iframehere', session=session,
      ui = div(
        id = "iframediv",
        tags$iframe(srcdoc=htmlpage_, height="100%", width="100%"))
    )
    ReactVals$Iframe_loaded <- TRUE
    
    insertUI(
      selector = '#Buckethere', session=session,
      ui = div(
        id = "sortable", Sortable_ui(pooled_items=ReactVals$cfg_$XPATH_settings[ReactVals$cfg_$inactiveXPATH],
                                     active_items=ReactVals$cfg_$XPATH_settings[ReactVals$cfg_$activeXPATH],
                                     session=session)
      )
    )
    ReactVals$Sortable_loaded <- TRUE
    
    
    #Reset or keep xpath settings
    if(ifelse(is.na(domain(cfg_$url)),"",domain(cfg_$url)) ==
       ifelse(is.na(domain(ReactVals$Previous_URL)),"",domain(ReactVals$Previous_URL)) |
       ifelse(is.na(domain(ReactVals$Previous_URL)),"",domain(ReactVals$Previous_URL)) == "") {
      
      # Domains are the same or config file was loaded, retain xpath settings
      add_xpath_rule('#sortable_pool',  ReactVals$cfg_$XPATH_settings[ReactVals$cfg_$inactiveXPATH], session)
      add_xpath_rule('#sortable_active',ReactVals$cfg_$XPATH_settings[ReactVals$cfg_$activeXPATH]  , session)
      
    } else if (ifelse(is.na(domain(ReactVals$Previous_URL)),"",domain(ReactVals$Previous_URL)) != "") { 
      
      # Settings will be reset
      showNotification("DOMAIN HAS CHANGED. XPATH SETTINGS WILL BE RESET",'',type = "message")
    }
    ReactVals$Previous_URL <- cfg_$url
    
    
  } # else if(isvalidstatus & isvalidcontent)
  
} # load_url_page
